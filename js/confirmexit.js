if (Drupal.jsEnabled) {
    $(document).ready(function() {
        var el = document.getElementById('node-form');
        var allowConfirm = true;
        window.onbeforeunload = confirmExit;
        el.addEventListener("click", _submit_node_form, false);
        
        function _submit_node_form(){
            allowConfirm = false;
        }
        
        function confirmExit(){
            if(allowConfirm)
                return Drupal.settings.confirmexit.message;
            else
                allowConfirm = true;            
        }
        
    });
    
}

